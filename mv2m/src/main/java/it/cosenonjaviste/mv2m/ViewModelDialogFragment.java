package it.cosenonjaviste.mv2m;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * TODO CLASS_DESCRIPTION
 */

public abstract class ViewModelDialogFragment<VM extends ViewModel<?, ?>> extends DialogFragment implements ViewModelContainer<VM> {

    protected VM viewModel;
    private ViewModelManager<VM> vmManager;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        vmManager = new ViewModelManager<>();
        viewModel = vmManager.getOrCreate(this, state);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        vmManager.saveState(outState);
    }

    public String getFragmentTag(Object args) {
        return getClass().getName() + "_" + args;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        vmManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        vmManager.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        vmManager.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        vmManager.destroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        vmManager.onCreateOptionsMenu(menu, inflater);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return vmManager.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}
