/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screens.authorisation;

import android.content.Context;
import android.os.Vibrator;
import android.widget.Toast;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.PinViewModel;
import sk.technologystudio.mobile.sample.ui.base.BaseActivity;
import sk.technologystudio.mobile.sample.R;

/**
 * Class displaying simple edit text for unlocking the application, accepts any input with length
 * greater than 4 characters
 */

public class PinActivity extends BaseActivity<PinViewModel> {

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            Vibrator v = (Vibrator) PinActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(300);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pin;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, R.string.enter_pin, Toast.LENGTH_SHORT).show();
    }

    @Override
    public PinViewModel createViewModel() {
        return App.getComponent(this).getPinViewModel();
    }
}
