/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screens.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.sample.ui.base.BaseFragmentActivity;
import sk.technologystudio.mobile.sample.ui.screenFlow.FragmentPool;
import sk.technologystudio.mobile.sample.R;
import sk.technologystudio.mobile.framework.core.base.VoidViewModel;
import sk.technologystudio.mobile.framework.model.managers.navigationFlow.NavigationFlowManager;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;

public class MenuFragmentActivity extends BaseFragmentActivity<VoidViewModel> {
    @Inject
    NavigationFlowManager mNavigationFlowManager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_lock:
                getPermissionManager().removeFeature(Features.PERMISSION_AUTHORIZED);
                getNavigator().screenHasPermissions(this, provideFragmentType().getArgs());
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getComponent(this).inject(this);
    }

    @NonNull
    @Override
    protected int getFragmentResId() {
        return R.id.activity_fragment;
    }

    @Nullable
    @Override
    public Bundle provideFragmentArguments() {
        return null;
    }

    @NonNull
    @Override
    public FragmentPool provideFragmentType() {
        return FragmentPool.MENU_FRAGMENT;
    }

    @NonNull
    @Override
    protected int getLayoutId() {
        return R.layout.activity_menu;
    }

    @Override
    public VoidViewModel createViewModel() {
        return new VoidViewModel();
    }
}
