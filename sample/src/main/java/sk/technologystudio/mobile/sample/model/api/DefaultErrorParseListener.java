/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api;

import android.support.annotation.Nullable;

import it.cosenonjaviste.mv2m.ActivityHolder;
import sk.technologystudio.mobile.sample.model.api.responses.SampleResponse;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.OnErrorParsedListener;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/27/17.
 */

public class DefaultErrorParseListener implements OnErrorParsedListener<SampleResponse> {

    private MessageDispatcher mMessageDispatcher;
    private MessageFactory mMessageFactory;
    private ActivityHolder mActivityHolder;

    public DefaultErrorParseListener(MessageDispatcher messageDispatcher,
                                     MessageFactory messageFactory,
                                     ActivityHolder activityHolder){

        mMessageDispatcher = messageDispatcher;
        mMessageFactory = messageFactory;
        mActivityHolder = activityHolder;
    }

    @Override
    public void onErrorParsed(@Nullable SampleResponse errorBody) {
        mMessageDispatcher.showMessage(mActivityHolder, mMessageFactory.getMessage(errorBody));
    }

    @Override
    public void onBadConnection(Throwable throwable) {
        mMessageDispatcher.showMessage(mActivityHolder, mMessageFactory.getMessage(throwable));
    }
}
