/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation.activation;

import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.framework.core.base.BaseModel;
import sk.technologystudio.mobile.framework.ui.utils.ObsField;

@ParcelablePlease
public class ActivateModel extends BaseModel implements Parcelable {

    ObsField name = new ObsField();
    ObsField password = new ObsField();

    public ObsField getName() {
        return name;
    }

    public ObsField getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ActivateModelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<ActivateModel> CREATOR = new Creator<ActivateModel>() {
        public ActivateModel createFromParcel(Parcel source) {
            ActivateModel target = new ActivateModel();
            ActivateModelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public ActivateModel[] newArray(int size) {
            return new ActivateModel[size];
        }
    };
}
