/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.menu;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import rx.Observable;
import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.core.utils.RxHelpers;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.HttpErrorHandler;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.OnErrorParsedListener;
import sk.technologystudio.mobile.sample.model.api.ApiService;
import sk.technologystudio.mobile.sample.model.api.DefaultErrorParseListener;
import sk.technologystudio.mobile.sample.model.api.responses.SampleResponse;
import sk.technologystudio.mobile.sample.ui.screenFlow.ActivityPool;

/**
 * TODO CLASS_DESCRIPTION
 */
public class MenuViewModel extends BaseViewModel<Void, MenuModel> {

    private ApiService mApiService;
    private Navigator mNavigator;
    private Validator mValidator;
    private HttpErrorHandler mErrorHandler;

    @Inject
    public MenuViewModel(ApiService service, Navigator navigator,
                         LoadingInteractor loadingInteractor,
                         Validator validator,
                         HttpErrorHandler errorHandler,
                         MessageDispatcher messageDispatcher,
                         MessageFactory messageFactory,
                         DeviceManager deviceManager) {
        super(loadingInteractor, deviceManager, messageDispatcher, messageFactory);
        mApiService = service;
        mNavigator = navigator;
        mValidator = validator;
        mErrorHandler = errorHandler;
    }

    @NonNull
    @Override
    protected MenuModel createModel() {
        return new MenuModel();
    }

    @Override
    public void resume() {
        super.resume();
        if (model.getItems().isEmpty()) {
            loadData();
        }
    }

    private void loadData() {
        Observable<SampleResponse> results = mApiService.getSampleData();
        results.compose(RxHelpers.networkSetup(getModel().loading, mValidator))
                .map(SampleResponse::getData)
                .subscribe(items -> model.items.addAll(items),
                        throwable -> mErrorHandler.handleError(
                                throwable,
                                getErrorListener(),
                                SampleResponse.class
                        )
                );
    }

    private OnErrorParsedListener<SampleResponse> getErrorListener() {
        return new DefaultErrorParseListener(mMessageDispatcher,
                mMessageFactory, activityHolder);
    }

    public void onClick(Integer position) {
        String item = model.getItems().get(position);
        mNavigator.openScreen(activityHolder, ActivityPool.DETAIL, item, false);
    }
}
