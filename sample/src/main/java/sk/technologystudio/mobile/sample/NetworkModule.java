/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample;

import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import config.ApiConfig;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sk.technologystudio.mobile.sample.model.api.ApiService;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;
import sk.technologystudio.mobile.framework.model.realm.settings.RealmExclusionStrategy;
import sk.technologystudio.mobile.framework.ui.utils.GsonUtils;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 2/24/17.
 */

@Module
public class NetworkModule {
    /**
     * This class should provide creation of API service and provide it to ViewModels
     * Using retrofit 2 as a standard, with logging level set to display body
     *
     * @param serviceClass Retrofit or similar
     * @param <T>          Generic type so you can use any type of API implementation
     * @return Retrofit service
     */
    private static <T> T createService(Class<T> serviceClass, Retrofit retrofit) {
        return retrofit.create(serviceClass);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient httpClient) {
        String baseUrl = ApiConfig.BASE_URL;
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {

        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setExclusionStrategies(new RealmExclusionStrategy())
                .registerTypeAdapterFactory(GsonUtils.IntObjectAdapterFactory.INSTANCE)
                .registerTypeAdapterFactory(GsonUtils.StringObjectAdapterFactory.INSTANCE)
                .create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttp(SessionManager sessionManager) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    okhttp3.Request.Builder request = chain.request().newBuilder();
                    request.addHeader("X-Application-Version",
                            String.valueOf(BuildConfig.VERSION_CODE))
                            .addHeader("X-Platform", "android")
                            .addHeader("X-Platform-Version",
                                    String.valueOf(Build.VERSION.SDK_INT));
                    if(sessionManager.getAccessToken() != null){
                        request.addHeader("Authorization",
                                "Bearer " + sessionManager.getAccessToken());
                    }
                    return chain.proceed(request.build());
                })
                .addInterceptor(logging).build();
    }


    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit) {
        return createService(ApiService.class, retrofit);
    }
}