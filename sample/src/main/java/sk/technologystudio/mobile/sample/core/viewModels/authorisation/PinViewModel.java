/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation;

import android.databinding.Observable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.model.managers.permission.SampleFeatures;
import sk.technologystudio.mobile.framework.R;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.core.utils.RxHelpers;
import sk.technologystudio.mobile.sample.model.api.ApiService;
import sk.technologystudio.mobile.sample.model.api.requests.AuthenticateRequest;
import sk.technologystudio.mobile.sample.model.api.responses.AuthenticateResponse;
import sk.technologystudio.mobile.sample.model.api.responses.AuthorizeResponse;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.HttpErrorHandler;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.OnErrorParsedListener;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManager;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

public class PinViewModel extends BaseViewModel<AuthorizeResponse, PinModel> implements
        OnErrorParsedListener<AuthorizeResponse> {

    private Navigator mNavigator;
    private SessionManager mSessionManager;
    private ApiService mApiService;
    private HttpErrorHandler mErrorHandler;
    private PermissionManager mPermissionManager;
    private Validator mValidator;

    private Observable.OnPropertyChangedCallback mCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable observable, int i) {
            if (getModel().getPin().text.get().length() > 3) {
                String refreshToken = mSessionManager.getRefreshToken(getModel().getPin().text.get());

                if (refreshToken == null) {
                    getModel().getPin().error.set(R.string.bad_pin);
                    getModel().getPin().text.clear();
                    getModel().getPin().notifyChange();
                } else {
                    mApiService.postAuthentification(createAuthenticateRequest(refreshToken))
                            .compose(RxHelpers.networkSetup(getModel().loading, mValidator))
                            .subscribe(PinViewModel.this::onAuthenticateResponse,
                                    throwable -> mErrorHandler.handleError(
                                            throwable,
                                            PinViewModel.this,
                                            AuthorizeResponse.class
                                    ));
                }
            }
        }
    };

    @Inject
    public PinViewModel(Navigator navigator,
                        LoadingInteractor loadingInteractor,
                        SessionManager sessionManager,
                        ApiService apiService,
                        HttpErrorHandler errorHandler,
                        DeviceManager deviceManager,
                        PermissionManager permissionManager,
                        MessageDispatcher messageDispatcher,
                        MessageFactory messageFactory,
                        Validator validator) {
        super(loadingInteractor, deviceManager, messageDispatcher, messageFactory);
        mNavigator = navigator;
        mSessionManager = sessionManager;
        mApiService = apiService;
        mErrorHandler = errorHandler;
        mPermissionManager = permissionManager;
        mValidator = validator;
    }

    private AuthenticateRequest createAuthenticateRequest(String refreshToken) {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest();
        authenticateRequest.setClientId(mDeviceManager.getClientId());
        authenticateRequest.setClientSecret(mDeviceManager.getClientSecret());
        authenticateRequest.setDeviceId(mDeviceManager.getDeviceId());
        authenticateRequest.setDeviceName(mDeviceManager.getDeviceName());
        authenticateRequest.setDeviceType(mDeviceManager.getDeviceType());
        authenticateRequest.setRefreshToken(refreshToken);
        return authenticateRequest;
    }

    private void onAuthenticateResponse(AuthenticateResponse response) {
        mSessionManager.setRefreshToken(response.getData().getRefreshToken(),
                getModel().getPin().text.get());
        mSessionManager.setAccessToken(response.getData().getAccessToken());
        mPermissionManager.addFeature(SampleFeatures.PERMISSION_AUTHORIZED);
        mNavigator.finishAlternativeFlow(activityHolder);
    }

    @NonNull
    @Override
    protected PinModel createModel() {
        return new PinModel();
    }

    @Override
    public void resume() {
        super.resume();
        getModel().getPin().text.addOnPropertyChangedCallback(mCallback);
    }

    @Override
    public void pause() {
        super.pause();
        getModel().getPin().text.removeOnPropertyChangedCallback(mCallback);
    }

    @Override
    public void onErrorParsed(@Nullable AuthorizeResponse errorBody) {
        getModel().getPin().error.set(R.string.bad_pin);
        getModel().getPin().text.clear();
        getModel().getPin().notifyChange();
    }

    @Override
    public void onBadConnection(Throwable throwable) {
        mMessageDispatcher.showMessage(activityHolder, mMessageFactory.getMessage(throwable));
        getModel().getPin().error.set(0);
        getModel().getPin().text.clear();
        getModel().getPin().notifyChange();
    }
}
