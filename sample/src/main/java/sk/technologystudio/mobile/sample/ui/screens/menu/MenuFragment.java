/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screens.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.cosenonjaviste.mv2m.ViewModelFragment;
import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.sample.BR;
import sk.technologystudio.mobile.sample.R;
import sk.technologystudio.mobile.sample.core.viewModels.menu.MenuViewModel;
import sk.technologystudio.mobile.sample.databinding.FragmentMenuBinding;
import sk.technologystudio.mobile.sample.databinding.ItemContactBinding;
import sk.technologystudio.mobile.framework.ui.utils.RecyclerBindingBuilder;


/**
 * TODO CLASS_DESCRIPTION
 */
public class MenuFragment extends ViewModelFragment<MenuViewModel> {

    FragmentMenuBinding mBinding;

    @Override
    public MenuViewModel createViewModel() {
        return App.getComponent(this).getMenuViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentMenuBinding.inflate(inflater);
        return new RecyclerBindingBuilder<String>(inflater, viewModel, R.id.question_detail, mBinding)
                .viewHolder(ItemContactBinding::inflate,
                        ItemContactBinding::setContact,
                        (position) -> viewModel.onClick(position)).getRoot();

    }
}
