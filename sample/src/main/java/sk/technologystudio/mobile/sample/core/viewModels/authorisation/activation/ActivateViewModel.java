/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation.activation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.ui.screenFlow.ActivityPool;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.core.utils.RxHelpers;
import sk.technologystudio.mobile.sample.model.api.ApiService;
import sk.technologystudio.mobile.sample.model.api.requests.AuthorizeRequest;
import sk.technologystudio.mobile.sample.model.api.responses.AuthorizeResponse;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.HttpErrorHandler;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.OnErrorParsedListener;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;

public class ActivateViewModel extends BaseViewModel<Void, ActivateModel>
        implements OnErrorParsedListener<AuthorizeResponse> {

    private ApiService mApiService;
    private HttpErrorHandler mErrorHandler;
    private Navigator mNavigator;
    private SessionManager mSessionManager;
    private Validator mValidator;

    @Inject
    public ActivateViewModel(ApiService apiService,
                             DeviceManager deviceManager,
                             HttpErrorHandler errorHandler,
                             Navigator navigator,
                             SessionManager sessionManager,
                             LoadingInteractor loadingInteractor,
                             MessageDispatcher messageDispatcher,
                             Validator validator, MessageFactory messageFactory) {
        super(loadingInteractor, deviceManager, messageDispatcher, messageFactory);
        mApiService = apiService;
        mErrorHandler = errorHandler;
        mNavigator = navigator;
        mSessionManager = sessionManager;
        mValidator = validator;
    }

    @NonNull
    @Override
    protected ActivateModel createModel() {
        return new ActivateModel();
    }

    public void register() {
        mApiService.postAuthorisation(createRequest())
                .compose(RxHelpers.networkSetup(getModel().loading, mValidator))
                .subscribe(this::handleResponse,
                        throwable -> mErrorHandler.handleError(throwable,
                        this, AuthorizeResponse.class));
    }

    private AuthorizeRequest createRequest() {
        AuthorizeRequest authorizeRequest = new AuthorizeRequest();
        authorizeRequest.setClientId(mDeviceManager.getClientId());
        authorizeRequest.setClientSecret(mDeviceManager.getClientSecret());
        authorizeRequest.setDeviceId(mDeviceManager.getDeviceId());
        authorizeRequest.setDeviceName(mDeviceManager.getDeviceName());
        authorizeRequest.setDeviceType(mDeviceManager.getDeviceType());
        authorizeRequest.setPassword(getModel().password.text.get());
        authorizeRequest.setUsername(getModel().name.text.get());
        return authorizeRequest;
    }

    private void handleResponse(AuthorizeResponse response){
        mSessionManager.setLoginName(getModel().getName().text.get());
        mSessionManager.setLoginPassword(getModel().getPassword().text.get());
        mNavigator.openScreen(activityHolder, ActivityPool.MENU, null, false);
    }

    public void forgotPassword() {

    }

    public void emergencyCall() {

    }

    public void showFirstAidCard() {

    }

    public void openDemo() {

    }

    @Override
    public void onErrorParsed(@Nullable AuthorizeResponse errorBody) {
        if (errorBody != null) {
            mNavigator.openScreen(activityHolder, ActivityPool.FINISH_ACTIVATION, errorBody, true);
        }
    }

    @Override
    public void onBadConnection(Throwable throwable) {
        mMessageDispatcher.showMessage(activityHolder, mMessageFactory.getMessage(throwable));
    }
}
