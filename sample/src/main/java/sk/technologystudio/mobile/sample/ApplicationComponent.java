/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample;

import javax.inject.Singleton;

import dagger.Component;
import sk.technologystudio.mobile.framework.ui.base.BaseActivity;
import sk.technologystudio.mobile.sample.core.viewModels.SplashViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.PinViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.activation.ActivateViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.finishActivation.FinishActivationViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.login.LoginViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.detail.DetailViewModel;
import sk.technologystudio.mobile.sample.core.viewModels.menu.MenuViewModel;
import sk.technologystudio.mobile.sample.ui.screens.SplashActivity;
import sk.technologystudio.mobile.sample.ui.screens.authorisation.PinActivity;
import sk.technologystudio.mobile.sample.ui.screens.menu.MenuFragmentActivity;
/**
 * TODO CLASS_DESCRIPTION
 */


@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface ApplicationComponent {

    MenuViewModel getMenuViewModel();

    SplashViewModel getSplashViewModel();

    DetailViewModel getDetailViewModel();

    ActivateViewModel getActivateViewModel();

    FinishActivationViewModel getFinishActivationViewModel();

    LoginViewModel getLoginViewModel();

    PinViewModel getPinViewModel();

    void inject(MenuFragmentActivity activity);

    void inject(PinActivity activity);

    void inject(BaseActivity.UtilityWrapper utilityWrapper);

    void inject(SplashActivity splashActivity);
}
