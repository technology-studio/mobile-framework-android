/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screenFlow;

import android.support.v4.app.Fragment;

import sk.technologystudio.mobile.sample.model.managers.permission.SampleFeatures;
import sk.technologystudio.mobile.sample.ui.screens.detail.DetailFragment;
import sk.technologystudio.mobile.sample.ui.screens.menu.MenuFragment;
import sk.technologystudio.mobile.framework.ui.base.BaseFragmentPool;

/**
 * Created by Juraj Oceliak{juraj.oceliak@eman.cz} on 2/24/17.
 */

public enum FragmentPool implements BaseFragmentPool {
    DETAIL_FRAGMENT(DetailFragment.class, SampleFeatures.PERMISSION_ACTIVATED, SampleFeatures.PERMISSION_AUTHORIZED),
    MENU_FRAGMENT(MenuFragment.class, SampleFeatures.PERMISSION_ACTIVATED, SampleFeatures.PERMISSION_AUTHORIZED);

    private Class<? extends Fragment> mClazz;
    private int[] mArgs;


    FragmentPool(Class<? extends Fragment> clazz, @SampleFeatures int... args) {
        mClazz = clazz;
        mArgs = args;
    }

    public Class<? extends Fragment> getFragmentClass() {
        return mClazz;
    }

    public void setFragmentClass(Class<? extends Fragment> mClazz) {
        this.mClazz = mClazz;
    }

    @SampleFeatures
    public int[] getArgs() {
        return mArgs;
    }

    public void setArgs(int[] mArgs) {
        this.mArgs = mArgs;
    }
}
