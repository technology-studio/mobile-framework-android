/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.base;

import it.cosenonjaviste.mv2m.ViewModel;
import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.sample.BR;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 4/4/17.
 */

public abstract class BaseActivity<VM extends ViewModel<?, ?>>
        extends sk.technologystudio.mobile.framework.ui.base.BaseActivity<VM> {
    @Override
    public void injectDependencies(UtilityWrapper wrapper) {
        App.getComponent(this).inject(wrapper);
    }

    @Override
    protected int getBrId() {
        return BR.viewModel;
    }
}
