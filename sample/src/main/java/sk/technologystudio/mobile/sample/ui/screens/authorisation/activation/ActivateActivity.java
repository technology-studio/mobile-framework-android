/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screens.authorisation.activation;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.sample.R;
import sk.technologystudio.mobile.sample.core.viewModels.authorisation.activation.ActivateViewModel;
import sk.technologystudio.mobile.sample.ui.base.BaseActivity;

public class ActivateActivity extends BaseActivity<ActivateViewModel> {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_activate;
    }

    @Override
    public ActivateViewModel createViewModel() {
        return App.getComponent(this).getActivateViewModel();
    }
}
