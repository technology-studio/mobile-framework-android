/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.detail;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import it.cosenonjaviste.mv2m.ViewModel;

/**
 * Detail view of selected item
 * String - body describing selected item passed as argument to viewModel
 */
public class DetailViewModel extends ViewModel<String, DetailModel> {

    @Inject
    public DetailViewModel() {
    }

    @Override
    public void resume() {
        super.resume();
    }

    @NonNull
    @Override
    protected DetailModel createModel() {
        DetailModel model = new DetailModel();
        model.getName().set(getArgument());
        model.getText().set(getArgument());
        return model;
    }
}
