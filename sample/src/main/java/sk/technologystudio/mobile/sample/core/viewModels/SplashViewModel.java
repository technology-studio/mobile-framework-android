/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.ui.screenFlow.ActivityPool;
import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/8/17.
 */

public class SplashViewModel extends BaseViewModel<Void, SplashModel> {

    private Navigator mNavigator;

    @Inject
    public SplashViewModel(LoadingInteractor loadingInteractor,
                           Navigator navigator) {
        super(loadingInteractor);
        mNavigator = navigator;
    }

    @Override
    public void resume() {
        super.resume();
        mNavigator.openScreen(activityHolder, ActivityPool.MENU, null, true);
    }

    @NonNull
    @Override
    protected SplashModel createModel() {
        return new SplashModel();
    }
}
