/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation.finishActivation;

import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.framework.core.base.BaseModel;
import sk.technologystudio.mobile.framework.ui.utils.ObsField;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/1/17.
 */

@ParcelablePlease
public class FinishActivationModel extends BaseModel implements Parcelable {

    ObsField mGridCode = new ObsField();
    ObsField mNewPin = new ObsField();
    ObsField mCheckPin = new ObsField();
    ObservableField<String> mTitle = new ObservableField<>();

    public ObsField getGridCode() {
        return mGridCode;
    }

    public ObsField getNewPin() {
        return mNewPin;
    }

    public ObsField getCheckPin() {
        return mCheckPin;
    }

    public ObservableField<String> getTitle() {
        return mTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        FinishActivationModelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<FinishActivationModel> CREATOR = new Creator<FinishActivationModel>() {
        public FinishActivationModel createFromParcel(Parcel source) {
            FinishActivationModel target = new FinishActivationModel();
            FinishActivationModelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public FinishActivationModel[] newArray(int size) {
            return new FinishActivationModel[size];
        }
    };
}
