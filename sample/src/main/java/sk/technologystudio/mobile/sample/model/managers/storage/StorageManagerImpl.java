/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.managers.storage;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.framework.model.managers.storage.StorageManager;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/3/17.
 */

public class StorageManagerImpl implements StorageManager {

    private static final String PREFS_NAME = "shared_preferences";
    private SharedPreferences mSharedPreferences;

    @Inject
    public StorageManagerImpl() {
    }

    @Override
    public void storeString(String value, String key) {
        getSharedPreferences().edit().putString(key, value).apply();
    }

    @Override
    public String getString(String key) {
        return getSharedPreferences()
                .getString(key, "");
    }

    @Override
    public void storeLong(long value, String key) {
        getSharedPreferences().edit().putLong(key, value).apply();
    }

    @Override
    public Long getLong(String key) {
        return getSharedPreferences()
                .getLong(key, 0);
    }

    @Override
    public void storeBoolean(boolean value, String key) {
        getSharedPreferences().edit().putBoolean(key, value).apply();
    }

    @Override
    public Boolean getBoolean(String key) {
        return getSharedPreferences()
                .getBoolean(key, false);
    }

    private SharedPreferences getSharedPreferences() {
        if(mSharedPreferences == null){
            mSharedPreferences = App.getInstance()
                    .getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }
}

