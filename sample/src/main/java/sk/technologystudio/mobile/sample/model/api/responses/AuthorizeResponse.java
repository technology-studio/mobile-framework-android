/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.framework.model.api.validator.Validable;
import sk.technologystudio.mobile.framework.model.factory.Messageable;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/1/17.
 */

@ParcelablePlease
public class AuthorizeResponse implements Parcelable, Messageable, Validable {

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("code")
    @Expose
    Integer code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("data")
    @Expose
    GridCardError data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GridCardError getData() {
        return data;
    }

    public void setData(GridCardError data) {
        this.data = data;
    }

    @Override
    public boolean isValid() {
        return getCode() != null
                && !getStatus().isEmpty()
                && !getMessage().isEmpty()
                && getData() != null;
    }

    @ParcelablePlease
    public static class GridCardError implements Parcelable {

        @SerializedName("type")
        @Expose
        String type;
        @SerializedName("gridcard_position")
        @Expose
        String gridcardPosition;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getGridcardPosition() {
            return gridcardPosition;
        }

        public void setGridcardPosition(String gridcardPosition) {
            this.gridcardPosition = gridcardPosition;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            AuthorizeResponse$GridCardErrorParcelablePlease.writeToParcel(this, dest, flags);
        }

        public static final Creator<GridCardError> CREATOR = new Creator<GridCardError>() {
            public GridCardError createFromParcel(Parcel source) {
                GridCardError target = new GridCardError();
                AuthorizeResponse$GridCardErrorParcelablePlease.readFromParcel(target, source);
                return target;
            }

            public GridCardError[] newArray(int size) {
                return new GridCardError[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        AuthorizeResponseParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<AuthorizeResponse> CREATOR = new Creator<AuthorizeResponse>() {
        public AuthorizeResponse createFromParcel(Parcel source) {
            AuthorizeResponse target = new AuthorizeResponse();
            AuthorizeResponseParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public AuthorizeResponse[] newArray(int size) {
            return new AuthorizeResponse[size];
        }
    };
}
