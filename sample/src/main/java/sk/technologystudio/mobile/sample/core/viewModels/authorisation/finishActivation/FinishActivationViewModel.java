/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation.finishActivation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import sk.technologystudio.mobile.framework.R;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.core.utils.RxHelpers;
import sk.technologystudio.mobile.sample.model.api.ApiService;
import sk.technologystudio.mobile.sample.model.api.requests.GridCodeRequest;
import sk.technologystudio.mobile.sample.model.api.responses.AuthorizeResponse;
import sk.technologystudio.mobile.sample.model.api.responses.GridCodeResponse;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.HttpErrorHandler;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.OnErrorParsedListener;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManager;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/1/17.
 */

public class FinishActivationViewModel extends BaseViewModel<AuthorizeResponse,
        FinishActivationModel> implements OnErrorParsedListener<AuthorizeResponse> {

    private Navigator mNavigator;
    private PermissionManager mPermissionManager;
    private ApiService mApiService;
    private HttpErrorHandler mErrorHandler;
    private SessionManager mSessionManager;
    private Validator mValidator;

    @Inject
    public FinishActivationViewModel(Navigator navigator,
                                     PermissionManager permissionManager,
                                     DeviceManager deviceManager,
                                     ApiService apiService,
                                     HttpErrorHandler errorHandler,
                                     SessionManager sessionManager,
                                     LoadingInteractor loadingInteractor,
                                     MessageDispatcher messageDispatcher,
                                     MessageFactory messageFactory,
                                     Validator validator) {
        super(loadingInteractor, deviceManager, messageDispatcher, messageFactory);
        mNavigator = navigator;
        mPermissionManager = permissionManager;
        mApiService = apiService;
        mErrorHandler = errorHandler;
        mSessionManager = sessionManager;
        mValidator = validator;
    }

    @NonNull
    @Override
    protected FinishActivationModel createModel() {
        FinishActivationModel model = new FinishActivationModel();
        String msg = "";
        if(getArgument() != null && getArgument().getData() != null){
            msg = getArgument().getData().getType();
        }
        model.getTitle().set(msg + " "
                + getArgument().getData().getGridcardPosition());
        return model;
    }

    public void finishRegistration() {
        String pin = getModel().getNewPin().text.get();
        String pin2 = getModel().getCheckPin().text.get();

        if (!pin.equals(pin2)) {
            getModel().getCheckPin().error.set(R.string.pins_must_match);
        } else if (pin.length() != 4 || pin2.length() != 4) {
            getModel().getCheckPin().error.set(R.string.pin_invalid_length);
        } else {
            register();
        }

        getModel().getCheckPin().notifyChange();
    }

    private void register() {
        mApiService.postGridCode(createRequest())
                .compose(RxHelpers.networkSetup(getModel().loading, mValidator))
                .subscribe(this::performPostRegistrationAction,
                        throwable -> mErrorHandler.handleError(throwable,
                                this, AuthorizeResponse.class));
    }

    private GridCodeRequest createRequest() {
        GridCodeRequest authorizeRequest = new GridCodeRequest();
        authorizeRequest.setClientId(mDeviceManager.getClientId());
        authorizeRequest.setClientSecret(mDeviceManager.getClientSecret());
        authorizeRequest.setDeviceId(mDeviceManager.getDeviceId());
        authorizeRequest.setDeviceName(mDeviceManager.getDeviceName());
        authorizeRequest.setDeviceType(mDeviceManager.getDeviceType());
        authorizeRequest.setPassword(mSessionManager.getLoginPassword());
        authorizeRequest.setUsername(mSessionManager.getLoginName());
        authorizeRequest.setGridCode(getModel().getGridCode().text.get());
        return authorizeRequest;
    }

    @SuppressWarnings("WrongConstant")
    private void performPostRegistrationAction(GridCodeResponse response) {
        mSessionManager.setAccessToken(response.getData().getAccessToken());
        mSessionManager.setRefreshToken(
                response.getData().getrefreshToken(),
                getModel().getCheckPin().text.get());
        mPermissionManager.addFeature(Features.PERMISSION_ACTIVATED
                | Features.PERMISSION_AUTHORIZED);
        mNavigator.finishAlternativeFlow(activityHolder);
    }

    @Override
    public void onErrorParsed(@Nullable AuthorizeResponse errorBody) {
        mMessageDispatcher.showMessage(activityHolder, mMessageFactory.getMessage(errorBody));
    }

    @Override
    public void onBadConnection(Throwable throwable) {
        mMessageDispatcher.showMessage(activityHolder, mMessageFactory.getMessage(throwable));
    }
}
