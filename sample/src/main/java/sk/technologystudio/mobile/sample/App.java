/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample;

import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import rx.plugins.RxJavaErrorHandler;
import rx.plugins.RxJavaPlugins;
import timber.log.Timber;

/**
 * Main application controller
 */
public class App extends Application {

    private static App sInstance;
    private ApplicationComponent component;

    /**
     * Static access to Application instance
     *
     * @return App instance
     */
    public static App getInstance() {
        return sInstance;
    }

    public static ApplicationComponent getComponent(Fragment fragment) {
        return getComponent(fragment.getActivity());
    }

    public static ApplicationComponent getComponent(Context context) {
        return (((App) context.getApplicationContext())).getComponent();
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent component) {
        this.component = component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        Realm.init(this);
        Timber.plant(new Timber.DebugTree());

        RxJavaPlugins.getInstance().registerErrorHandler(new RxJavaErrorHandler() {
            @Override
            public void handleError(Throwable e) {
                Timber.w(e, "RxError");
            }
        });

        component = DaggerApplicationComponent.builder()
                .appModule(new AppModule())
                .networkModule(new NetworkModule())
                .build();
    }
}
