/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.sample.model.managers.factory.MessageFactoryImpl;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractorImpl;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcherImpl;
import sk.technologystudio.mobile.framework.core.interactors.navigation.AppNavigator;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.sample.model.managers.device.DeviceManagerMock;
import sk.technologystudio.mobile.framework.model.managers.errorHandling.HttpErrorHandler;
import sk.technologystudio.mobile.framework.model.managers.navigationFlow.NavigationFlowManager;
import sk.technologystudio.mobile.sample.model.managers.navigationFlow.NavigationFlowManagerImpl;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManager;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManagerImpl;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManagerImpl;
import sk.technologystudio.mobile.framework.model.managers.storage.StorageManager;
import sk.technologystudio.mobile.sample.model.managers.storage.StorageManagerImpl;
import sk.technologystudio.mobile.framework.model.api.validator.ValidatorImpl;

/**
 * TODO CLASS_DESCRIPTION
 */

@Module
public class AppModule {


    @Provides
    @Singleton
    Navigator provideNavigator(PermissionManager permissionManager, NavigationFlowManager navigationFlowManager) {
        return new AppNavigator(permissionManager, navigationFlowManager);
    }


    @Provides
    @Singleton
    PermissionManager providePermissionManager(SessionManager sessionManager) {
        return new PermissionManagerImpl(sessionManager);
    }


    @Provides
    @Singleton
    DeviceManager provideDeviceManager() {
        return new DeviceManagerMock();
    }

    @Provides
    @Singleton
    HttpErrorHandler provideErrorHandler(Retrofit retrofit) {
        return new HttpErrorHandler(retrofit);
    }

    @Provides
    @Singleton
    SessionManager provideSessionManager(StorageManager storageManager) {
        return new SessionManagerImpl(storageManager);
    }

    @Provides
    @Singleton
    StorageManager provideStorageManager() {
        return new StorageManagerImpl();
    }

    @Provides
    @Singleton
    NavigationFlowManager provideFlowManager(SessionManager sessionManager) {
        return new NavigationFlowManagerImpl(sessionManager);
    }

    @Provides
    @Singleton
    LoadingInteractor provideLoadingInteractor() {
        return new LoadingInteractorImpl();
    }

    @Provides
    @Singleton
    MessageDispatcher provideMessageDispatcher() {
        return new MessageDispatcherImpl();
    }

    @Provides
    @Singleton
    MessageFactory provideMessageFactory(){
        return new MessageFactoryImpl();
    }

    @Provides
    @Singleton
    Validator provideValidator(){
        return new ValidatorImpl();
    }

}
