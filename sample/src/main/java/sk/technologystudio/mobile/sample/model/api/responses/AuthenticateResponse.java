/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import sk.technologystudio.mobile.framework.model.api.validator.Validable;
import sk.technologystudio.mobile.framework.model.factory.Messageable;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/1/17.
 */

public class AuthenticateResponse implements Messageable, Validable {
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("code")
    @Expose
    Integer code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("data")
    @Expose
    Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean isValid() {
        return getCode() != null
                && !getMessage().isEmpty()
                && getData() != null;
    }


    public static class Data {

        @SerializedName("refresh_token")
        @Expose
        String refreshToken;
        @SerializedName("access_token")
        @Expose
        String accessToken;

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

    }
}
