/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.managers.permission;

import javax.inject.Inject;

import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManagerImpl;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 4/6/17.
 */

public class PermissionManagerVszp extends PermissionManagerImpl {

    @Inject
    public PermissionManagerVszp(SessionManager sessionManager) {
        if (sessionManager.isActivated()) {
            addFeature(SampleFeatures.PERMISSION_ACTIVATED);
        }
    }
}
