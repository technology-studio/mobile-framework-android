/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.managers.factory;

import android.support.annotation.Nullable;

import java.net.UnknownHostException;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.framework.R;
import sk.technologystudio.mobile.framework.model.factory.Messageable;
import sk.technologystudio.mobile.framework.model.exceptions.InvalidServerObjectException;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/27/17.
 */

public class MessageFactoryImpl implements MessageFactory {
    @Override
    public String getMessage(@Nullable Throwable throwable) {
        String message = App.getInstance().getString(R.string.unknown_exception);

        if (throwable == null) {
            return message;
        }
        if (throwable instanceof UnknownHostException) {
            message = App.getInstance().getString(R.string.no_internet);
        } else if (throwable instanceof InvalidServerObjectException) {
            message = App.getInstance().getString(R.string.malformed_object);
        }
        return message;
    }

    @Override
    public String getMessage(@Nullable Messageable object) {
        if (object == null || object.getMessage() == null
                || object.getMessage().isEmpty()) {
            return App.getInstance().getString(R.string.unknown_exception);
        }
        return object.getMessage();
    }
}
