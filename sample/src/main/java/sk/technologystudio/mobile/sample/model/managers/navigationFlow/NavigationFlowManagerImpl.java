/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.managers.navigationFlow;

import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import sk.technologystudio.mobile.sample.ui.screenFlow.ActivityPool;
import sk.technologystudio.mobile.sample.ui.screens.authorisation.PinActivity;
import sk.technologystudio.mobile.sample.ui.screens.authorisation.activation.ActivateActivity;
import sk.technologystudio.mobile.framework.model.managers.navigationFlow.NavigationFlowManager;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;
import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 2/23/17.
 */

public class NavigationFlowManagerImpl implements NavigationFlowManager {

    private SessionManager mSessionManager;

    @Inject
    public NavigationFlowManagerImpl(SessionManager sessionManager) {
        mSessionManager = sessionManager;
    }

    @Override
    public Class<? extends AppCompatActivity> getRequiredFeatureActivity(@Features int missingFeatures) {
        if (((Features.PERMISSION_ACTIVATED | Features.PERMISSION_AUTHORIZED) & missingFeatures) > 0) {
            if (!mSessionManager.isActivated()) {
                return ActivateActivity.class;
            } else {
                return PinActivity.class;
            }
        }
        throw new IllegalStateException("NOT DEFINED FEATURE ACTIVITY");
    }

    @Override
    public Class<? extends AppCompatActivity> getLockScreenActivity() {
        return ActivityPool.LOCK_SCREEN.getActivityClass();
    }
}
