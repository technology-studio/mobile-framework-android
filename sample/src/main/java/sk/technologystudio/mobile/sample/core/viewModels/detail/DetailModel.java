/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.detail;

import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.framework.core.base.ListModel;
import sk.technologystudio.mobile.sample.model.api.models.Item;

/**
 * TODO CLASS_DESCRIPTION
 */
@ParcelablePlease
public class DetailModel extends ListModel<Item> implements Parcelable {

    ObservableField<String> mName = new ObservableField<>();
    ObservableField<String> mText = new ObservableField<>();

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getText() {
        return mText;
    }

    public void setPhone(ObservableField<String> phone) {
        this.mName = phone;
    }

    public void setText(String mText) {
        this.mText.set(mText);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        DetailModelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<DetailModel> CREATOR = new Creator<DetailModel>() {
        public DetailModel createFromParcel(Parcel source) {
            DetailModel target = new DetailModel();
            DetailModelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public DetailModel[] newArray(int size) {
            return new DetailModel[size];
        }
    };
}
