/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.managers.device;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import sk.technologystudio.mobile.sample.App;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;

public class DeviceManagerMock implements DeviceManager {
    @Override
    public String getClientId() {
        return "1234567890";
    }

    @Override
    public String getClientSecret() {
        return "ABCDEF123456789";
    }

    @Override
    public String getDeviceId() {
        return "ABCDEF123456789";
    }

    @Override
    public String getDeviceName() {
        return "User's phone";
    }

    @Override
    public String getDeviceType() {
        return "phone";
    }

    @Override
    public Long getCurrentTime() {
        return System.currentTimeMillis();
    }

    @Override
    public boolean hasInternetConnection() {
        ConnectivityManager connMgr = (ConnectivityManager)
                App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
