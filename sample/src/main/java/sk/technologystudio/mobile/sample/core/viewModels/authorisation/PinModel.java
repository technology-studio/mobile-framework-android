/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.core.viewModels.authorisation;

import android.databinding.ObservableField;
import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.sample.core.viewModels.authorisation.PinModelParcelablePlease;
import sk.technologystudio.mobile.framework.core.base.BaseModel;
import sk.technologystudio.mobile.framework.ui.utils.ObsField;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

@ParcelablePlease
public class PinModel extends BaseModel implements Parcelable {

    ObsField pin = new ObsField();
    ObservableField<String> title = new ObservableField<>();

    public ObsField getPin() {
        return pin;
    }

    public ObservableField<String> getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        PinModelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<PinModel> CREATOR = new Creator<PinModel>() {
        public PinModel createFromParcel(Parcel source) {
            PinModel target = new PinModel();
            PinModelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public PinModel[] newArray(int size) {
            return new PinModel[size];
        }
    };
}
