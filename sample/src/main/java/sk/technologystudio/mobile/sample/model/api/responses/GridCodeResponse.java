/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import sk.technologystudio.mobile.framework.model.api.validator.Validable;
import sk.technologystudio.mobile.framework.model.factory.Messageable;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

@ParcelablePlease
public class GridCodeResponse implements Parcelable, Messageable, Validable {

    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("code")
    @Expose
    Integer code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("data")
    @Expose
    AccessData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AccessData getData() {
        return data;
    }

    public void setData(AccessData data) {
        this.data = data;
    }

    @Override
    public boolean isValid() {
        return getCode() != null
                && !getStatus().isEmpty()
                && !getMessage().isEmpty()
                && getData() != null;
    }

    @ParcelablePlease
    public static class AccessData implements Parcelable {

        @SerializedName("access_token")
        @Expose
        String accessToken;
        @SerializedName("refresh_token")
        @Expose
        String refreshToken;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getrefreshToken() {
            return refreshToken;
        }

        public void setrefreshToken(String gridcardPosition) {
            this.refreshToken = gridcardPosition;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            GridCodeResponse$AccessDataParcelablePlease.writeToParcel(this, dest, flags);
        }

        public static final Creator<AccessData> CREATOR = new Creator<AccessData>() {
            public AccessData createFromParcel(Parcel source) {
                AccessData target = new AccessData();
                GridCodeResponse$AccessDataParcelablePlease.readFromParcel(target, source);
                return target;
            }

            public AccessData[] newArray(int size) {
                return new AccessData[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        GridCodeResponseParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<GridCodeResponse> CREATOR = new Creator<GridCodeResponse>() {
        public GridCodeResponse createFromParcel(Parcel source) {
            GridCodeResponse target = new GridCodeResponse();
            GridCodeResponseParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public GridCodeResponse[] newArray(int size) {
            return new GridCodeResponse[size];
        }
    };
}
