/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;
import sk.technologystudio.mobile.sample.model.api.responses.GridCodeResponse;
import sk.technologystudio.mobile.sample.model.api.requests.AuthenticateRequest;
import sk.technologystudio.mobile.sample.model.api.requests.AuthorizeRequest;
import sk.technologystudio.mobile.sample.model.api.requests.GridCodeRequest;
import sk.technologystudio.mobile.sample.model.api.responses.AuthenticateResponse;
import sk.technologystudio.mobile.sample.model.api.responses.AuthorizeResponse;
import sk.technologystudio.mobile.sample.model.api.responses.SampleResponse;

/**
 * Api interface defining calls
 */
public interface ApiService {
    @POST("device/authentication")
    Observable<AuthenticateResponse> postAuthentification(@Body AuthenticateRequest request);

    @POST("device/authorization")
    Observable<AuthorizeResponse> postAuthorisation(@Body AuthorizeRequest request);

    @POST("device/authorization?gridcode")
    Observable<GridCodeResponse> postGridCode(@Body GridCodeRequest request);

    @GET("sample")
    Observable<SampleResponse> getSampleData();

}
