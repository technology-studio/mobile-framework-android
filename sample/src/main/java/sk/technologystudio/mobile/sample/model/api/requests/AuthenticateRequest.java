/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.model.api.requests;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/1/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class AuthenticateRequest implements Parcelable {

    @SerializedName("client_id")
    @Expose
    String clientId;
    @SerializedName("client_secret")
    @Expose
    String clientSecret;
    @SerializedName("refresh_token")
    @Expose
    String refreshToken;
    @SerializedName("device_id")
    @Expose
    String deviceId;
    @SerializedName("device_name")
    @Expose
    String deviceName;
    @SerializedName("device_type")
    @Expose
    String deviceType;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        AuthenticateRequestParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<AuthenticateRequest> CREATOR = new Creator<AuthenticateRequest>() {
        public AuthenticateRequest createFromParcel(Parcel source) {
            AuthenticateRequest target = new AuthenticateRequest();
            AuthenticateRequestParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public AuthenticateRequest[] newArray(int size) {
            return new AuthenticateRequest[size];
        }
    };
}
