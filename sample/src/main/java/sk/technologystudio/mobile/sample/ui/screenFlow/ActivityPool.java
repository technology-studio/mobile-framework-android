/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.sample.ui.screenFlow;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import sk.technologystudio.mobile.sample.ui.screens.authorisation.PinActivity;
import sk.technologystudio.mobile.sample.ui.screens.authorisation.finishActivation.FinishActivationActivity;
import sk.technologystudio.mobile.sample.ui.screens.detail.DetailFragmentActivity;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;
import sk.technologystudio.mobile.sample.ui.screens.menu.MenuFragmentActivity;
import sk.technologystudio.mobile.framework.ui.base.BaseActivityPool;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 2/23/17.
 */

public enum ActivityPool implements BaseActivityPool {
    LOCK_SCREEN(PinActivity.class),
    MENU(MenuFragmentActivity.class, FragmentPool.MENU_FRAGMENT),
    DETAIL(DetailFragmentActivity.class, FragmentPool.DETAIL_FRAGMENT),
    FINISH_ACTIVATION(FinishActivationActivity.class);

    private Class<? extends AppCompatActivity> mClazz;
    private FragmentPool mFragment;
    private int [] mPermissions;

    ActivityPool(Class<? extends AppCompatActivity> clazz, @Nullable FragmentPool fragment) {
        mClazz = clazz;
        mFragment = fragment;
    }

    ActivityPool(Class<? extends AppCompatActivity> clazz, @Nullable @Features int ... permissions) {
        mClazz = clazz;
        mPermissions = permissions;
    }

    @Nullable
    public FragmentPool getFragment() {
        return mFragment;
    }

    public Class<? extends AppCompatActivity> getActivityClass() {
        return mClazz;
    }

    public int[] getRequiredPermissions() {
        if(mFragment != null) {
           return mFragment.getArgs();
        }
        return mPermissions;
    }
}
