/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.realm.settings;

import io.realm.Realm;
import io.realm.RealmObject;
import rx.Observable;
import rx.Subscriber;
import sk.technologystudio.mobile.framework.model.realm.RealmConfig;

public class RxRealmCache {

    private RxRealmCache() {
        throw new AssertionError("No instances");
    }

    public static <T extends RealmObject> Observable.Transformer<T, T> cache(final Class<T> clazz) {
        return source -> Observable
                .create(new Observable.OnSubscribe<T>() {
                    @Override
                    public void call(Subscriber<? super T> subscriber) {
                        Realm realm = Realm.getInstance(RealmConfig.CACHE.getConfiguration());
                        T cache = realm.where(clazz).findFirst();
                        if (cache != null) {
                            subscriber.onNext(cache);
                        }
                        subscriber.onCompleted();
                    }
                })
                .concatWith(source)
                .map(t -> {
                    Realm realm = Realm.getInstance(RealmConfig.CACHE.getConfiguration());
                    realm.beginTransaction();
                    T managed = realm.copyToRealmOrUpdate(t);
                    realm.commitTransaction();
                    return managed;
                });
    }

}
