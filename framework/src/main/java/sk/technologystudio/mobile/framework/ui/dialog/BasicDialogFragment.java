/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.StringRes;

import sk.technologystudio.mobile.framework.R;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 4/25/17.
 */

public class BasicDialogFragment extends DialogFragment {

    public static BasicDialogFragment newInstance(@StringRes int okButton, @StringRes int title,
                                           @StringRes int message, boolean dismissable) {
        BasicDialogFragment f = new BasicDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("ok", okButton);
        args.putInt("title", title);
        args.putInt("message", message);
        args.putBoolean("dis", dismissable);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(getArguments().getBoolean("dis"));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getInt("title"))
                .setMessage(getArguments().getInt("message"))
                .setPositiveButton(getArguments().getInt("ok"), (dialog1, which) -> {
                    if (getActivity() instanceof BaseDialogListener) {
                        ((BaseDialogListener) getActivity()).onPositiveButtonClicked();
                    } else {
                        throw  new IllegalStateException("Base activity must extend BaseDialogListener");
                    }
                    if(isAdded()) {
                        dismiss();
                    }
                }).create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().getAttributes().windowAnimations
                    = R.style.DialogAnimation;
        }
    }
}
