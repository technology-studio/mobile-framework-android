/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.managers.session;

import javax.inject.Inject;

import sk.technologystudio.mobile.framework.model.managers.storage.StorageManager;
import sk.technologystudio.mobile.framework.model.utils.Encryption;
import timber.log.Timber;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

public class SessionManagerImpl implements SessionManager {
    public static final String REFRESH_TOKEN = "refresh_token";
    private String mLoginName;
    private String mLoginPassword;
    private String mRefreshToken;
    private String mAccessToken;
    private String mPin;
    private StorageManager mStorageManager;

    @Inject
    public SessionManagerImpl(StorageManager storageManager) {
        mStorageManager = storageManager;
    }

    public String getPin() {
        return mPin;
    }    @Override
    public void setLoginName(String loginName) {
        mLoginName = loginName;
    }

    public void setPin(String pin) {
        mPin = pin;
    }



    @Override
    public void setLoginPassword(String loginPassword) {

        mLoginPassword = loginPassword;
    }

    @Override
    public String getLoginName() {
        return mLoginName;
    }

    @Override
    public String getLoginPassword() {
        return mLoginPassword;
    }

    @Override
    public void setRefreshToken(String refreshToken, String pin) {
        mPin = pin;
        mRefreshToken = refreshToken;
        encryptAndStoreToken(pin, refreshToken);
    }

    @Override
    public void setAccessToken(String accessToken) {

        mAccessToken = accessToken;
    }

    @Override
    public String getRefreshToken(String pin) {
        mPin = pin;
        decryptStoredToken(pin);
        return mRefreshToken;
    }

    private void encryptAndStoreToken(String pin, String refreshToken) {
        String encrypted = null;
        try {
            encrypted = Encryption.encrypt(pin + pin.hashCode(), refreshToken);
        } catch (Exception e) {
            Timber.e(e, "encryption failed");
        }
        mStorageManager.storeString(encrypted, REFRESH_TOKEN);
    }

    private void decryptStoredToken(String pin) {
        if (mRefreshToken == null) {
            try {
                mRefreshToken = Encryption.decrypt(pin + pin.hashCode(),
                        mStorageManager.getString(REFRESH_TOKEN));
            } catch (Exception e) {
                Timber.e(e, "decryption failed");
            }
        }
    }

    @Override
    public String getAccessToken() {
        return mAccessToken;
    }

    @Override
    public boolean isActivated() {
        return !mStorageManager.getString(REFRESH_TOKEN).isEmpty();
    }
}
