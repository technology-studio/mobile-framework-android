/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.utils;

import android.support.annotation.Nullable;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Utility class for parsing error response json into specific class T
 */
public class ErrorUtils {

    private ErrorUtils() {
    }

    @Nullable
    public static <T> T parseError(Class<T> clazz, Response<?> response, Retrofit retrofit) {
        Converter<ResponseBody, T> converter =
                retrofit.responseBodyConverter(clazz, new Annotation[0]);

        T error = null;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            try {
                return clazz.newInstance();
            } catch (InstantiationException e1) {
                Timber.e(e1, "Can't create instance of errorBody");
            } catch (IllegalAccessException e1) {
                Timber.e(e1, "Can't create instance of errorBody");
            }
        } catch (JsonSyntaxException jse) {
            try {
                return clazz.newInstance();
            } catch (InstantiationException e) {
                Timber.e(e, "Can't create instance of errorBody");
            } catch (IllegalAccessException e) {
                Timber.e(e, "Can't create instance of errorBody");
            }
        }

        return error;
    }
}