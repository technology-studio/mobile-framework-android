/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import it.cosenonjaviste.mv2m.ArgumentManager;
import it.cosenonjaviste.mv2m.ViewModel;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;

/**
 * Base activity from which other activties should extend
 * we can define global behaviour here
 */
public abstract class BaseFragmentActivity<VM extends ViewModel<?, ?>> extends BaseActivity<VM> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(getFragmentResId(), createFragment()).commit();
        }
    }

    public void replaceFragment(BaseFragmentPool fragment, @Nullable Object arg, boolean addToBackstack){
        FragmentManager manager = getSupportFragmentManager();
        Fragment frag = Fragment.instantiate(this,
                fragment.getFragmentClass().getName(),
                ArgumentManager.writeArgument(new Bundle(), arg));
        FragmentTransaction replace = manager.beginTransaction().replace(getFragmentResId(), frag);
        if (addToBackstack){
            replace.addToBackStack(frag.getClass().getName());
        }
        replace.commit();
    }

    @IdRes
    protected abstract int getFragmentResId();

    private Fragment createFragment() {
        return Fragment.instantiate(this,
                provideFragmentType().getFragmentClass().getName(),
                provideFragmentArguments());
    }


    @Nullable
    public abstract Bundle provideFragmentArguments();

    @NonNull
    public abstract BaseFragmentPool provideFragmentType();

    @Features
    public int[] getRequiredFeatures() {
        return provideFragmentType().getArgs();
    }
}
