
/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;


public class ObservableInteger extends BaseObservable implements Parcelable {
    private int mValue = 0;

    public ObservableInteger(int value) {
        this.mValue = value;
    }

    public ObservableInteger() {
    }

    protected ObservableInteger(Parcel in) {
        mValue = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObservableInteger> CREATOR = new Creator<ObservableInteger>() {
        @Override
        public ObservableInteger createFromParcel(Parcel in) {
            return new ObservableInteger(in);
        }

        @Override
        public ObservableInteger[] newArray(int size) {
            return new ObservableInteger[size];
        }
    };

    public int get() {
        return this.mValue;
    }

    public void set(int value) {
        if (value != this.mValue) {
            this.mValue = value;
            this.notifyChange();
        }

    }
}