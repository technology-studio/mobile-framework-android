/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.binding;

import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.databinding.ObservableInt;
import android.databinding.adapters.ListenerUtil;
import android.support.design.widget.TextInputLayout;
import android.support.v4.util.Pair;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import sk.technologystudio.mobile.framework.R;
import sk.technologystudio.mobile.framework.ui.utils.ObsField;
import sk.technologystudio.mobile.framework.ui.utils.ObservableBool;
import sk.technologystudio.mobile.framework.ui.utils.ObservableString;
import sk.technologystudio.mobile.framework.ui.utils.TextWatcherAdapter;
import timber.log.Timber;

/**
 * Converters for databiding
 */
public class DataBindingConverters {

    @BindingAdapter({"app:imageUrl"})
    public static void loadImage(ImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext()).load(url).placeholder(R.drawable.ic_mood_black_24dp).into(view);
        } else {
            view.setImageDrawable(null);
        }
    }

    /**
     * Binds focus, text and error message from edit text to observable
     *
     * @param view  View to be binded to viewModel
     * @param field Observable binded to
     */

    @BindingAdapter("bind")
    public static void bindAllToEditText(EditText view, final ObsField field) {
        bindEditText(view, field.text);
        bindFocusToEditText(view, field.focus);
        bindValidationError(view, field.error);
    }

    @BindingAdapter("android:text")
    public static void bindEditText(EditText view,
                                    final ObservableString observableString) {

        Pair pair = (Pair) view.getTag(R.id.binded);

        if (pair == null || pair.first != observableString) {
            if (pair != null) {
                view.removeTextChangedListener((TextWatcher) pair.second);
            }
            TextWatcherAdapter watcher;
            watcher = new TextWatcherAdapter() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    observableString.set(s.toString());
                }
            };
            view.setTag(R.id.binded, new Pair<>(observableString, watcher));
            view.addTextChangedListener(watcher);
        }

        String newValue = observableString.get();
        if (!view.getText().toString().equals(newValue)) {
            view.setText(newValue);
        }
    }

    /**
     * Binds focus listener to edit text, which updates observable object that is monitored in viewModel
     *
     * @param view  Edit text that user interacts with
     * @param focus Custom class holding focus boolean
     */

    @BindingAdapter("focus")
    public static void bindFocusToEditText(EditText view,
                                           final ObservableBool focus) {
        final View.OnFocusChangeListener newListener = ((v, hasFocus) -> focus.set(hasFocus));

        //Tool to prevent double binding of listener
        final View.OnFocusChangeListener oldListener = ListenerUtil.trackListener(view,
                newListener, R.id.focusBinded);
        if (oldListener != null) {
            view.setOnFocusChangeListener(null);
        }
        if (newListener != null) {
            view.setOnFocusChangeListener(newListener);
        }
    }

    /**
     * Translates string reference into text for view
     *
     * @param view
     * @param errorResId
     */
    @BindingAdapter({"error"})
    public static void bindValidationError(EditText view, ObservableInt errorResId) {
        if (view.getParent().getParent() instanceof TextInputLayout) {
            ((TextInputLayout) view.getParent().getParent()).setError(errorResId.get() == 0
                    ? null
                    : view.getResources().getString(errorResId.get())
            );
        } else {
            view.setError(errorResId.get() == 0
                    ? null
                    : view.getResources().getString(errorResId.get()));
        }
    }

    @BindingAdapter({"format", "arg"})
    public static void setFormattedText(TextView textView, int format, Object ... argId) {
        if (argId[0] instanceof Integer) {
            try {
                textView.setText(textView.getResources().getQuantityString(format, (Integer) argId[0], argId));
                return;
            } catch (Resources.NotFoundException nfe) {
                Timber.v("Plurals resources not found, falling back to classical formatter");
            }
        }
        textView.setText(Html.fromHtml(String.format(textView.getResources().getString(format), argId)));
    }

    @BindingAdapter({"format", "arg1", "arg2"})
    public static void setFormattedTextTwoArgs(TextView textView, int format, Object argId1, Object argId2) {
        textView.setText(Html.fromHtml(String.format(textView.getResources().getString(format), argId1, argId2)));
    }
}
