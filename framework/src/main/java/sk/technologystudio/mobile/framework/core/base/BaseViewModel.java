/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.base;

import android.databinding.Observable;
import android.os.Parcelable;

import java.net.UnknownHostException;

import it.cosenonjaviste.mv2m.ViewModel;
import sk.technologystudio.mobile.framework.core.interactors.loading.LoadingInteractor;
import sk.technologystudio.mobile.framework.core.interactors.message.MessageDispatcher;
import sk.technologystudio.mobile.framework.model.factory.MessageFactory;
import sk.technologystudio.mobile.framework.model.managers.device.DeviceManager;
import sk.technologystudio.mobile.framework.ui.utils.ObsField;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

public abstract class BaseViewModel<A, M extends BaseModel & Parcelable> extends ViewModel<A, M> {

    private LoadingInteractor mLoadingInteractor;
    protected DeviceManager mDeviceManager;
    protected MessageDispatcher mMessageDispatcher;
    protected MessageFactory mMessageFactory;
    private boolean mWithoutInternetDetection;

    private Observable.OnPropertyChangedCallback mLoadingListener = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable observable, int i) {
            mLoadingInteractor.setLoading(activityHolder,
                    getModel().loading.get());
        }
    };

    public BaseViewModel(LoadingInteractor loadingInteractor, DeviceManager deviceManager,
                         MessageDispatcher messageDispatcher, MessageFactory messageFactory) {
        mLoadingInteractor = loadingInteractor;
        mDeviceManager = deviceManager;
        mMessageDispatcher = messageDispatcher;
        mMessageFactory = messageFactory;
    }

    public BaseViewModel(LoadingInteractor loadingInteractor) {
        mLoadingInteractor = loadingInteractor;
        mWithoutInternetDetection = true;
    }

    @Override
    public void pause() {
        super.pause();
        getModel().loading.removeOnPropertyChangedCallback(mLoadingListener);
    }

    @Override
    public void resume() {
        super.resume();
        getModel().loading.addOnPropertyChangedCallback(mLoadingListener);
        if (!mWithoutInternetDetection && !mDeviceManager.hasInternetConnection()) {
            mMessageDispatcher.showIndefiniteMessage(activityHolder,
                    mMessageFactory.getMessage(new UnknownHostException()));
        }
    }

    public void setFieldText(ObsField field, String text){
        field.text.set(text);
        field.notifyChange();
    }
}
