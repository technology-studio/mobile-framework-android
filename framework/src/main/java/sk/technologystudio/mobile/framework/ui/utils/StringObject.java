/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class StringObject extends RealmObject implements Parcelable {

    public String mValue;

    public StringObject() {
    }

    public StringObject(String value) {
        mValue = value;
    }

    protected StringObject(Parcel in) {
        mValue = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StringObject> CREATOR = new Creator<StringObject>() {
        @Override
        public StringObject createFromParcel(Parcel in) {
            return new StringObject(in);
        }

        @Override
        public StringObject[] newArray(int size) {
            return new StringObject[size];
        }
    };

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

}
