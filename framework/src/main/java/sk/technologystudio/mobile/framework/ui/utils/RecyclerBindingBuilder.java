/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.databinding.ViewDataBinding;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.cosenonjaviste.mv2m.ViewModel;
import it.cosenonjaviste.mv2m.recycler.BindableAdapter;
import it.cosenonjaviste.mv2m.recycler.BindableViewHolder;
import rx.functions.Action1;
import rx.functions.Func3;
import sk.technologystudio.mobile.framework.core.base.ListModel;

/**
 * Class that should provide mBinding for recyclerView
 * Needs additional tweaking for use case of various types of items
 *
 * @param <T> Generic type for item class to be injected into view
 */

public class RecyclerBindingBuilder<T> {

    private final LayoutInflater inflater;

    private final ViewModel<?, ? extends ListModel<T>> viewModel;
    private int mLayout;

    private ViewDataBinding mBinding;

    private BindableAdapter.ViewHolderFactory<T> factory;

    /**
     * Constructor providing classic inflater to inflate selected class
     * currently set specifically for main activity, needs to be generalized for any type of layout
     *  @param inflater  Inflated, it would be good idea to replace it by already inflated view
     * @param viewModel ViewModel class according to MVVM architecture, in XML has to be defined as "viewModel" variable name
     */
    public RecyclerBindingBuilder(LayoutInflater inflater, ViewModel viewModel,
                                  @IdRes int layout, ViewDataBinding binding) {
        this.inflater = inflater;
        this.viewModel = viewModel;
        mLayout = layout;
        mBinding = binding;
    }

    /**
     * @return Generated class of layout by Databinding android library
     */

    public ViewDataBinding getBinding() {
        if (((RecyclerView) mBinding.getRoot().findViewById(mLayout)).getLayoutManager() == null) {
            linearLayoutManager();
        }
        return mBinding;
    }

    /**
     * @return Parent class for #setContentView in fragment/activity
     */
    public View getRoot() {
        return getBinding().getRoot();
    }

    /**
     * @return layout manager, can be customized
     */
    public RecyclerBindingBuilder<T> linearLayoutManager() {
        ((RecyclerView) mBinding.getRoot().findViewById(mLayout)).setLayoutManager(new LinearLayoutManager(mBinding.getRoot().getContext()));
        return this;
    }

    private <B extends ViewDataBinding> RecyclerBindingBuilder<T> viewHolderWithCustomizer(
            Func3<LayoutInflater, ViewGroup, Boolean, B> inflateFunction,
            BindableViewHolder.Binder<B, T> binder,
            Action1<BindableViewHolder<T>> customizer) {
        factory = v -> {
            B binding = inflateFunction.call(inflater, v, false);
            BindableViewHolder<T> viewHolder = BindableViewHolder.create(binding, binder);
            if (customizer != null) {
                customizer.call(viewHolder);
            }
            return viewHolder;
        };

        ((RecyclerView) mBinding.getRoot().findViewById(mLayout)).setAdapter(getNewAdapter());
        return this;
    }

    @NonNull
    public BindableAdapter<T> getNewAdapter() {
        return new BindableAdapter<>(viewModel.getModel().getItems(), factory);
    }

    public <B extends ViewDataBinding> RecyclerBindingBuilder<T> viewHolder(Func3<LayoutInflater, ViewGroup, Boolean, B> inflateFunction, BindableViewHolder.Binder<B, T> binder) {
        return viewHolderWithCustomizer(inflateFunction, binder, null);
    }

    public <B extends ViewDataBinding> RecyclerBindingBuilder<T> viewHolder(
            Func3<LayoutInflater, ViewGroup, Boolean, B> inflateFunction,
            BindableViewHolder.Binder<B, T> binder,
            Action1<Integer> clickListener) {
        return viewHolderWithCustomizer(inflateFunction, binder, vh -> vh.itemView.setOnClickListener(v -> clickListener.call(vh.getAdapterPosition())));
    }
}
