/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.app.Activity;
import android.content.Intent;

import it.cosenonjaviste.mv2m.ViewModel;
import it.cosenonjaviste.mv2m.ViewModelContainer;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 5/11/17.
 */

public abstract class ViewModelContainerAdapter<VM extends ViewModel<?,?>> implements ViewModelContainer<VM> {
    @Override
    public VM createViewModel() {
        return null;
    }

    @Override
    public String getFragmentTag(Object args) {
        return null;
    }

    @Override
    public Activity getActivity() {
        return null;
    }

    @Override
    public void startActivity(Intent intent) {

    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {

    }
}
