/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.utils;

import android.databinding.ObservableArrayList;
import android.os.Parcel;

import com.hannesdorfmann.parcelableplease.ParcelBagger;

public class ObservableArrayListBagger implements ParcelBagger<ObservableArrayList> {
    @Override public void write(ObservableArrayList value, Parcel out, int flags) {
        out.writeList(value);
    }

    @Override public ObservableArrayList read(Parcel in) {
        ObservableArrayList observableArrayList = new ObservableArrayList();
        in.readList(observableArrayList, observableArrayList.getClass().getClassLoader());
        return observableArrayList;
    }
}
