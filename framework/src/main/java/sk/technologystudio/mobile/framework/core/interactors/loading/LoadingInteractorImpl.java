/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.interactors.loading;

import it.cosenonjaviste.mv2m.ActivityHolder;
import sk.technologystudio.mobile.framework.ui.base.BaseActivity;
import timber.log.Timber;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/3/17.
 */

public class LoadingInteractorImpl implements LoadingInteractor {
    @Override
    public void setLoading(ActivityHolder activityHolder, boolean showLoading) {
        if (activityHolder.getActivity() instanceof BaseActivity) {
            if (showLoading) {
                ((BaseActivity) activityHolder.getActivity()).showLoadingProgress();
            } else {
                ((BaseActivity) activityHolder.getActivity()).hideLoadingProgress();
            }

        } else {
            Timber.w("Activity doesn't extend base class, can't show loading progress");
        }
    }
}
