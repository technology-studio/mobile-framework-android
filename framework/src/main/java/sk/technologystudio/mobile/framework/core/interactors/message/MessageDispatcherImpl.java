/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.interactors.message;

import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import it.cosenonjaviste.mv2m.ActivityHolder;
import sk.technologystudio.mobile.framework.ui.dialog.BasicDialogFragment;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/27/17.
 */

public class MessageDispatcherImpl implements MessageDispatcher {
    @Override
    public void showMessage(ActivityHolder activityHolder, String message) {
        showSnackBar(activityHolder, message, false);
    }

    @Override
    public void showAlertMessage(ActivityHolder activityHolder,
                                 @StringRes int title,
                                 @StringRes int message,
                                 @StringRes int positiveButton,
                                 boolean dismissable) {

        if (activityHolder.getActivity() == null) {
            return;
        }

        BasicDialogFragment basicDialogFragment;
        basicDialogFragment = BasicDialogFragment
                .newInstance(positiveButton, title, message, dismissable);
        basicDialogFragment.show(activityHolder.getActivity().getFragmentManager(),
                activityHolder.getActivity().getString(message));
    }

    @Override
    public void showIndefiniteMessage(ActivityHolder activityHolder, String message) {
        showSnackBar(activityHolder, message, true);
    }

    private void showSnackBar(ActivityHolder activityHolder, String message, boolean indefinite) {
        if (activityHolder.getActivity() != null
                && activityHolder.getActivity().getWindow() != null
                && activityHolder.getActivity().getWindow().getCurrentFocus() != null) {

            Snackbar snack = Snackbar.make(activityHolder.getActivity()
                            .getWindow().getCurrentFocus(),
                    message,
                    indefinite ? Snackbar.LENGTH_INDEFINITE : Snackbar.LENGTH_LONG);

            View view = snack.getView();
            ViewGroup.LayoutParams params = view.getLayoutParams();

            if (params instanceof CoordinatorLayout.LayoutParams) {
                ((CoordinatorLayout.LayoutParams) params).gravity = Gravity.TOP;
            }

            if (params instanceof FrameLayout.LayoutParams) {
                ((FrameLayout.LayoutParams) params).gravity = Gravity.TOP;
            }

            view.setLayoutParams(params);
            snack.show();
        }
    }
}
