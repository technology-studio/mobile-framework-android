/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * PermissionActivity.java
 *
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Activity which should be called once we need any permission
 *
 */
public class PermissionActivity extends AppCompatActivity {

    private static final String ARG_PERMISSION = "_permission";
    private static final String ARG_PERMISSIONS = "_permissions";
    private static final String TAG = "PermissionActivity";

    /**
     * Creates intent
     *
     * @param ctx        context
     * @param permission one permission that is requested
     * @return a prepared intent
     */
    public static Intent getIntent(@NonNull Context ctx, @NonNull String permission) {
        Intent intent = new Intent(ctx, PermissionActivity.class);
        intent.putExtra(ARG_PERMISSION, permission);
        return intent;
    }

    /**
     * Creates intent
     *
     * @param ctx         context
     * @param permissions list of permissions that are requested
     * @return a prepared intent
     */
    public static Intent getIntent(@NonNull Context ctx, @NonNull String[] permissions) {
        Intent intent = new Intent(ctx, PermissionActivity.class);
        intent.putExtra(ARG_PERMISSIONS, permissions);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String perms[];
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(ARG_PERMISSIONS) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                perms = getIntent().getExtras().getStringArray(ARG_PERMISSIONS);

                if (perms != null) {
                    if (perms.length > 0) {
                        requestPermissions(perms, 0);
                    } else {
                        Log.e(TAG, "Permission request is not valid - ARG_PERMISSIONS array is empty");
                        finish();
                    }
                } else {
                    Log.e(TAG, "Permission request is not valid - ARG_PERMISSIONS array is null");
                    finish();
                }
            } else if (getIntent().getExtras().containsKey(ARG_PERMISSION) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String perm = getIntent().getExtras().getString(ARG_PERMISSION);

                if (perm != null) {
                    requestPermissions(new String[]{perm}, 0);
                } else {
                    Log.e(TAG, "Permission request is not valid - ARG_PERMISSION is null");
                    finish();
                }
            } else {
                Log.e(TAG, "Permission does not contain ARG_PERMISSIONS or ARG_PERMISSION or device is not running Android 6.0+");
                finish(); // I suppose there is no reason to run this activity without permission request or on old devices
            }
        } else {
            Log.e(TAG, "Permission does not contain ARG_PERMISSIONS or ARG_PERMISSION");
            finish(); // I suppose there is no reason to run this activity without permission request or on devices
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "One or more permission were not granted, finishing");
                setResult(Activity.RESULT_CANCELED);
                finish();
                return;
            }
        }

        setResult(Activity.RESULT_OK);
        finish();
    }
}