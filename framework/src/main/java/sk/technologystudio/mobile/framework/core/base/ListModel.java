/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.base;

import android.databinding.ObservableArrayList;

import com.hannesdorfmann.parcelableplease.annotation.Bagger;

import sk.technologystudio.mobile.framework.core.utils.ObservableArrayListBagger;

public abstract class ListModel<T> extends BaseModel {

    @Bagger(ObservableArrayListBagger.class)
    public ObservableArrayList<T> items = new ObservableArrayList<>();

    public final ObservableArrayList<T> getItems() {
        return items;
    }
}