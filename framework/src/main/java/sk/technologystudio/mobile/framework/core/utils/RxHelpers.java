/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.utils;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sk.technologystudio.mobile.framework.model.api.validator.Validable;
import sk.technologystudio.mobile.framework.model.api.validator.Validator;
import sk.technologystudio.mobile.framework.model.exceptions.InvalidServerObjectException;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

public class RxHelpers {

    private RxHelpers() {
    }

    public static <T> Observable.Transformer<T, T> networkSetup(@NonNull ObservableBoolean loading,
                                                                @NonNull Validator validator) {
        return source -> source.compose(schedulers())
                .compose(validateResponse(validator))
                .compose(loader(loading));
    }

    public static <T> Observable.Transformer<T, T> schedulers() {
        return source -> source.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> Observable.Transformer<T, T> validateResponse(Validator validator) {
        return source -> source.map(t -> {
            if (t instanceof Validable) {
                if (!validator.isValid((Validable) t)) {
                    throw new InvalidServerObjectException(t.getClass().getName() + " is not valid");
                }
            }
            return t;
        });
    }

    public static <T> Observable.Transformer<T, T> loader(ObservableBoolean loading) {
        return source -> source.doOnSubscribe(() -> loading.set(true))
                .doOnCompleted(() -> loading.set(false))
                .doOnError((throwable) -> loading.set(false));
    }
}
