/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.interactors.navigation;

import android.app.Activity;
import android.support.annotation.Nullable;

import it.cosenonjaviste.mv2m.ActivityHolder;
import sk.technologystudio.mobile.framework.ui.base.BaseActivityPool;
import sk.technologystudio.mobile.framework.ui.base.BaseFragmentPool;

/**
 * Class delegating android actions from
 */
public interface Navigator {
    void openScreen(ActivityHolder activityHolder, BaseActivityPool activity, @Nullable  Object arg,
                    boolean finishCurrentActivity);

    void changeFragment(ActivityHolder activityHolder, BaseFragmentPool fragment,
                        @Nullable Object arg, boolean addToBackStack);

    void finishAlternativeFlow(ActivityHolder activityHolder);

    boolean screenHasPermissions(Activity activity, int ... permissions);
}
