/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package sk.technologystudio.mobile.framework.ui.utils;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.annimon.stream.Objects;

/**
 * Custom class needed because {@link android.databinding.ObservableBoolean} doesn't provide two way
 * databinding
 */

public class ObservableBool extends BaseObservable implements Parcelable{
    private boolean mValue = false;

    public ObservableBool(boolean value) {
        this.mValue = value;
    }

    public ObservableBool() {
    }

    protected ObservableBool(Parcel in) {
        mValue = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mValue ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObservableBool> CREATOR = new Creator<ObservableBool>() {
        @Override
        public ObservableBool createFromParcel(Parcel in) {
            return new ObservableBool(in);
        }

        @Override
        public ObservableBool[] newArray(int size) {
            return new ObservableBool[size];
        }
    };

    public boolean get() {
        return this.mValue;
    }

    public void set(boolean value) {
        if (!Objects.equals(value, this.mValue)) {
            this.mValue = value;
            this.notifyChange();
        }

    }
}