/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.managers.errorHandling;

import java.net.UnknownHostException;

import javax.inject.Inject;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import sk.technologystudio.mobile.framework.model.utils.ErrorUtils;
import timber.log.Timber;

/**
 * Actions to reduce mundane tasks
 */
public class HttpErrorHandler {
    private Retrofit retrofit;

    @Inject
    public HttpErrorHandler(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public <T> void  handleError(Throwable throwable, OnErrorParsedListener<T> listener, Class<T> clazz) {
        handleError(throwable, listener, clazz, false);
    }

    public <T> void handleError(Throwable throwable, OnErrorParsedListener<T> listener,
                                Class<T> clazz, boolean silent) {
        Timber.d(throwable, "HttpErrorHandler handled exception");
        if (throwable instanceof HttpException) {
            if(((HttpException) throwable).code()<500) {
                T errorBody
                        = ErrorUtils.parseError(clazz,
                        ((HttpException) throwable).response(), retrofit);
                listener.onErrorParsed(errorBody);
            } else {
                if (!silent) {
                    listener.onBadConnection(throwable);
                }
            }
        } else if (throwable instanceof UnknownHostException) {
            if (!silent) {
                listener.onBadConnection(throwable);
            }
        } else if (!silent) {
            listener.onBadConnection(throwable);
        }
    }
}
