/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.managers.session;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 3/2/17.
 */

public interface SessionManager {

    void setLoginName(String loginName);
    void setLoginPassword(String loginPassword);
    String getLoginName();
    String getLoginPassword();
    void setRefreshToken(String refreshToken, String pin);
    void setAccessToken(String accessToken);
    String getRefreshToken(String pin);
    String getAccessToken();

    boolean isActivated();
}
