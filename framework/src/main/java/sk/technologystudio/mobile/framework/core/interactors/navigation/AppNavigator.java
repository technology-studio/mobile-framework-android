/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.core.interactors.navigation;

import android.app.Activity;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import it.cosenonjaviste.mv2m.ActivityHolder;
import sk.technologystudio.mobile.framework.model.managers.navigationFlow.NavigationFlowManager;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManager;
import sk.technologystudio.mobile.framework.ui.base.BaseActivity;
import sk.technologystudio.mobile.framework.ui.base.BaseActivityPool;
import sk.technologystudio.mobile.framework.ui.base.BaseFragmentActivity;
import sk.technologystudio.mobile.framework.ui.base.BaseFragmentPool;

/**
 * Implementation of {@link Navigator}, handles logic of rerouting navigation flow based
 * on information from permission manager
 */
public class AppNavigator implements Navigator {

    private PermissionManager mPermissionManager;
    private NavigationFlowManager mNavigationFlowManager;
    private BaseActivityPool mActivityRedirectedFrom;
    private Object mActivityArgument;

    @Inject
    public AppNavigator(PermissionManager permissionManager, NavigationFlowManager navigationFlowManager) {
        mPermissionManager = permissionManager;
        mNavigationFlowManager = navigationFlowManager;
    }

    @Override
    public void openScreen(ActivityHolder activityHolder,
                           BaseActivityPool activity, @Nullable Object arg,
                           boolean finishCurrentActivity) {

        if (activityHolder.getActivity() == null) {
            return;
        }
        if (screenHasPermissions(activityHolder.getActivity(),
                activity.getRequiredPermissions())) {

            ((BaseActivity) activityHolder.getActivity())
                    .startActivity(activity.getActivityClass(), arg);

        } else {
            mActivityRedirectedFrom = activity;
            mActivityArgument = arg;
        }

        if (finishCurrentActivity) {
            activityHolder.finishActivity();
        }
    }

    @Override
    public void changeFragment(ActivityHolder activityHolder, BaseFragmentPool fragment, @Nullable Object arg, boolean addToBackStack) {
        if(activityHolder.getActivity() == null){
            return;
        }

        if(activityHolder.getActivity() instanceof BaseFragmentActivity){
            BaseFragmentActivity activity = (BaseFragmentActivity) activityHolder.getActivity();
            activity.replaceFragment(fragment, arg, addToBackStack);
        }
    }

    @Override
    public void finishAlternativeFlow(ActivityHolder activityHolder) {
        if (mActivityRedirectedFrom == null) {
            activityHolder.finishActivity();
            mActivityArgument = null;
            return;
        }

        openScreen(activityHolder, mActivityRedirectedFrom, mActivityArgument, true);
        mActivityRedirectedFrom = null;
        mActivityArgument = null;
    }

    public boolean screenHasPermissions(Activity activity, int ... permissions) {
        BaseActivity baseActivity = (BaseActivity) activity;
        if (permissions != null && !mPermissionManager.hasFeatures(permissions)) {
            baseActivity.startActivity(
                    mNavigationFlowManager
                            .getRequiredFeatureActivity(
                                    mPermissionManager.getLockedFeatures(
                                            permissions)),
                    activity.getClass().getName());
            return false;
        }
        return true;
    }
}
