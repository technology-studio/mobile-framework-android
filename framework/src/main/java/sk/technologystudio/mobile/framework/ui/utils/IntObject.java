/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.utils;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Object holding int for realm usage
 * Created by {juraj.oceliak@technologystudio.sk} on 6/6/16.
 */


public class IntObject extends RealmObject implements Parcelable {

    public IntObject(int value) {
        mValue = value;
    }

    public IntObject() {
    }

    int mValue;

    protected IntObject(Parcel in) {
        mValue = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IntObject> CREATOR = new Creator<IntObject>() {
        @Override
        public IntObject createFromParcel(Parcel in) {
            return new IntObject(in);
        }

        @Override
        public IntObject[] newArray(int size) {
            return new IntObject[size];
        }
    };

    public int getValue() {
        return mValue;
    }

    public void setValue(int value) {
        mValue = value;
    }

}
