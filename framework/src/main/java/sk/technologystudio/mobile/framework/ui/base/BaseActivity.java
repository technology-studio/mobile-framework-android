/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.ui.base;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.transition.Transition;
import android.support.v7.app.AppCompatActivity;
import android.transition.TransitionManager;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.inject.Inject;

import dagger.Lazy;
import it.cosenonjaviste.mv2m.ArgumentManager;
import it.cosenonjaviste.mv2m.ViewModel;
import it.cosenonjaviste.mv2m.ViewModelActivity;
import sk.technologystudio.mobile.framework.core.interactors.navigation.Navigator;
import sk.technologystudio.mobile.framework.model.managers.navigationFlow.NavigationFlowManager;
import sk.technologystudio.mobile.framework.model.managers.permission.Features;
import sk.technologystudio.mobile.framework.model.managers.permission.PermissionManager;
import sk.technologystudio.mobile.framework.ui.dialog.LoadingDialogFragment;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 2/28/17.
 */

public abstract class BaseActivity<VM extends ViewModel<?, ?>> extends ViewModelActivity<VM> {

    private ViewDataBinding mBinding;
    private UtilityWrapper mUtilityWrapper;
    private LoadingDialogFragment mLoadingDialogFragment;
    private boolean isLoadingShown = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUtilityWrapper = new UtilityWrapper();
        injectDependencies(mUtilityWrapper);
        mBinding = DataBindingUtil.setContentView(this, getLayoutId());
        if (mBinding != null) {
            mBinding.setVariable(getBrId(), viewModel);
            mBinding.executePendingBindings();
        }
        mLoadingDialogFragment = new LoadingDialogFragment();
    }

    public abstract void injectDependencies(UtilityWrapper wrapper);

    @LayoutRes
    protected abstract int getLayoutId();

    @IdRes
    protected abstract int getBrId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeActivityFromTransitionManager(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private static void removeActivityFromTransitionManager(Activity activity) {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        Class transitionManagerClass = TransitionManager.class;
        try {
            Field runningTransitionsField = transitionManagerClass.getDeclaredField("sRunningTransitions");
            runningTransitionsField.setAccessible(true);
            //noinspection unchecked
            ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> runningTransitions
                    = (ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>>)
                    runningTransitionsField.get(transitionManagerClass);
            if (runningTransitions.get() == null || runningTransitions.get().get() == null) {
                return;
            }
            ArrayMap map = runningTransitions.get().get();
            View decorView = activity.getWindow().getDecorView();
            if (map.containsKey(decorView)) {
                map.remove(decorView);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public Navigator getNavigator() {
        return mUtilityWrapper.getNavigatorLazy().get();
    }

    @Features
    public int[] getRequiredFeatures() {
        return new int[]{};
    }

    public PermissionManager getPermissionManager() {
        return mUtilityWrapper.getPermissionManagerLazy().get();
    }

    public void startActivity(Class<? extends AppCompatActivity> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

    public NavigationFlowManager getNavigationFlowManager() {
        return mUtilityWrapper.flowManagerLazy.get();
    }

    public void startActivity(Class<? extends AppCompatActivity> clazz, @Nullable Object arg) {
        Intent intent = new Intent(this, clazz);
        if (arg != null) {
            ArgumentManager.writeArgument(intent, arg);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getIntent() != null && transitionsEnabled()) {
            try {
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            } catch (NullPointerException exc) {
                startActivity(intent);
            }

        } else {
            startActivity(intent);
        }
    }

        public boolean transitionsEnabled() {
        return true;
    }

    public ViewDataBinding getBinding() {
        return mBinding;
    }

    public synchronized void showLoadingProgress() {
        if (!mLoadingDialogFragment.isAdded() && !mLoadingDialogFragment.isVisible() && !isLoadingShown) {
            mLoadingDialogFragment.show(getSupportFragmentManager(), "progDialog");
            isLoadingShown = true;
        }
    }

    public void hideLoadingProgress() {
        if (mLoadingDialogFragment.isAdded() && isLoadingShown) {
            mLoadingDialogFragment.dismiss();
            isLoadingShown = false;
        }
    }

    public static class UtilityWrapper {

        @Inject
        Lazy<PermissionManager> permissionManagerLazy;

        @Inject
        Lazy<Navigator> navigatorLazy;

        @Inject
        Lazy<NavigationFlowManager> flowManagerLazy;

        public Lazy<PermissionManager> getPermissionManagerLazy() {
            return permissionManagerLazy;
        }

        public Lazy<Navigator> getNavigatorLazy() {
            return navigatorLazy;
        }

        private Lazy<NavigationFlowManager> getFlowManagerLazy() {
            return flowManagerLazy;
        }
    }
}
