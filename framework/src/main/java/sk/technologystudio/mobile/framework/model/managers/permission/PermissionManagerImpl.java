/*
 *  Copyright 2017 Juraj Oceliak @ Technology Studio
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package sk.technologystudio.mobile.framework.model.managers.permission;

import javax.inject.Inject;

import sk.technologystudio.mobile.framework.model.managers.session.SessionManager;
import sk.technologystudio.mobile.framework.model.utils.HelperMethods;

/**
 * Created by Juraj Oceliak{juraj.oceliak@technologystudio.sk} on 2/23/17.
 */

public class PermissionManagerImpl implements PermissionManager {

    private boolean mHasFeature;
    private
    int mUnlockedFeatures;

    @Inject
    public PermissionManagerImpl(SessionManager sessionManager) {
        if(sessionManager.isActivated()){
            addFeature(Features.PERMISSION_ACTIVATED);
        }
    }

    public PermissionManagerImpl() {
    }

    //0101 - unlocked features
    //&
    //0011 - required features
    //=
    //0001
    //user has at least all the features in line b;

    @Override
    public boolean hasFeatures(@Features int[] requiredFeatures) {
        int features = HelperMethods.binarySumOfIntArray(requiredFeatures);

        return (features & getFeatures()) == features;
    }

    @Override
    public int getFeatures() {
        return mUnlockedFeatures;
    }

    @Override
    public void addFeature(int feature) {
        mUnlockedFeatures |= feature;
    }

    @SuppressWarnings("WrongConstant")
    @Override
    public void removeFeature(int feature) {
        if ((mUnlockedFeatures & feature) == feature) {
            mUnlockedFeatures -= feature;
        }
    }

    //0101 - unlocked features
    //^    - xor
    //0011 - required features
    //=
    //0110
    //&
    //0011 - we want to get required features that are missing only
    //=
    //0010 - our missing feature

    @Override
    public int getLockedFeatures(int[] requiredFeatures) {
        int sumOfRequiredFeatures = HelperMethods.binarySumOfIntArray(requiredFeatures);

        return (mUnlockedFeatures ^ sumOfRequiredFeatures) & sumOfRequiredFeatures;
    }

    @Override
    public boolean hasUserUnlockedFeature() {
        return mHasFeature;
    }

    @Override
    public void featureEnabled(boolean enabled) {
        mHasFeature = enabled;
    }


    public void setHasFeature(boolean mHasFeature) {
        this.mHasFeature = mHasFeature;
    }
}
