<?xml version="1.0"?>
<recipe>
  	<merge from="AndroidManifest.xml.ftl"
             to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />

	<instantiate from="src/app_package/Activity.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/ui/screens/${escapeXmlAttribute(funcPackage)}/${className}Activity.java" />
	<instantiate from="src/app_package/ViewModel.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/core/view_models/${escapeXmlAttribute(funcPackage)}/${className}ViewModel.java" />
	<instantiate from="src/app_package/Model.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/core/view_models/${escapeXmlAttribute(funcPackage)}/${className}Model.java" />
 
     	<instantiate from="res/layout/activity.xml.ftl"
             to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />
	<open file="${srcOut}/core/viewModels/${funcPackage}/${className}ViewModel.java"/>
</recipe>