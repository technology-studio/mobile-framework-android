package ${packageName}.ui.screens.${funcPackage};

import ${packageName}.App;
import ${packageName}.core.view_models.${funcPackage}.${className}ViewModel;
import ${packageName}.ui.base.BaseActivity;
import ${packageName}.R;

public class ${className}Activity extends BaseActivity<${className}ViewModel> {

    @Override
    protected int getLayoutId() {
        return R.layout.${layoutName};
    }

    @Override
    public ${className}ViewModel createViewModel() {
        return App.getComponent(this).get${className}ViewModel();
    }
}