package ${packageName}.core.view_models.${funcPackage};

import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;
import sk.technologystudio.mobile.framework.core.base.BaseModel;

@ParcelablePlease
public class ${className}Model extends BaseModel implements Parcelable {
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ${className}ModelParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<${className}Model> CREATOR = new Creator<${className}Model>() {
        public ${className}Model createFromParcel(Parcel source) {
            ${className}Model target = new ${className}Model();
            ${className}ModelParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public ${className}Model[] newArray(int size) {
            return new ${className}Model[size];
        }
    };
}