package ${packageName}.core.view_models.${funcPackage};

import sk.technologystudio.mobile.framework.core.base.BaseViewModel;
import android.support.annotation.NonNull;
import javax.inject.Inject;

public class ${className}ViewModel extends BaseViewModel<Void, ${className}Model> {


    @Inject
    public ${className}ViewModel() {
    }

    @NonNull
    @Override
    protected ${className}Model createModel() {
        return new ${className}Model();
    }
}