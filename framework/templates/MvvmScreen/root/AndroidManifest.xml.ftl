<manifest xmlns:android="http://schemas.android.com/apk/res/android" >

    <application>
        <activity android:name="${relativePackage}.ui.screens.${funcPackage}.${className}Activity" />
    </application>

</manifest>
